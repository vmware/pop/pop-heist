=========
POP Heist
=========

Pop-Heist is a fork of the Heist project, taking inspiration from its
capabilities and enhancing them with the powerful principles of
pluggability, extensibility, and maintainability. While Heist, widely
respected for its advancement on salt-ssh, was originally designed to
deploy a Salt binary on an arbitrary SSH target, Pop-Heist goes a step further
by integrating dedicated subsystems for managing connections, services,
artifacts, and target systems themselves.

These subsystems were present in Heist but became tightly coupled over time
with heist-salt, diminishing the original intent of creating distinct,
functional compartments. Through our experiences with idem, a robust,
plugin-driven distributed application, we've gained insights that have shaped
the development of Pop-Heist into an exemplary model of a pluggable ephemeral
software distribution system.

This term refers to a system designed for the temporary distribution and
management of software agents across a network. These agents exist only for
the duration of specific tasks or sessions, enhancing adaptability and
resource efficiency. In this way, Pop-Heist not only serves as a platform for
deploying and managing agents, including Salt Minions, but also opens up
avenues for broader applications, thanks to its expansive pluggability.


.. code-block:: bash

        git clone https://gitlab.com/vmware/pop/pop-heist.git
        pip install pop-heist


POP-Heist Subsystems
====================

Pop-Heist is architected around several distinct subsystems, designed to
cater to different facets of software distribution and management.
While the majority of Pop-Heist extensions will likely focus on extending
the "Manager" subsystem to deploy and manage a new specific service, the
architecture allows for external projects to extend all of these subsystems.
This design promotes enhanced flexibility and functionality in the software
distribution process.

- Tunnel: This subsystem handles connections to the target system. While the
default connection method is "asyncssh", plugins for other connection protocols
can be easily integrated. Examples could include "winrm", rpc, or any other
remote connection method. This allows Pop-Heist to adapt to varying network
environments and security requirements.

- Service: The Service subsystem is responsible for running the downloaded
binary as a service on the target system. It can support various service
models, be it a Windows service, a systemd service, init, or simply running
the binary in the background. Its pluggable nature makes it adaptable to
different operating system environments and service management standards.

- Artifact: The Artifact subsystem focuses on downloading the binary from the
source to the target. It initially employs a Python requests-based plugin,
but it's built with the flexibility to accommodate other downloading methods,
such as Artifactory, wget, etc. This flexibility ensures that Pop-Heist can
fetch binaries from a wide array of sources, thereby accommodating different
deployment workflows.

- Manager: The Manager subsystem orchestrates the entire process. It prepares
and configures the target to host a binary, including tasks such as gathering
information about the target OS, selecting the appropriate binary, and invoking
the right plugins in the correct sequence. It acts as the conductor, ensuring
the entire operation proceeds smoothly and efficiently.

By offering extensibility via plugins, each subsystem in Pop-Heist adapts to a
range of different scenarios and use-cases. This adaptability and versatility
underpin Pop-Heist's value proposition as a comprehensive, yet customizable,
ephemeral software distribution system.

Making Your Roster
==================

A Roster is a file used by Heist to map login information to the
systems in your environment. This file can be very simple and just
needs to tell Heist where your systems are and how to log into them
via ssh. Open a file called `roster.cfg` and add the data needed to connect
to a remote system via ssh:

.. code-block:: yaml

    system_name:
      host: 192.168.4.4
      username: fred
      password: freds_password

.. note::

    This example is very simple, heist supports virtually all available authentication
    options for ssh.


The roster files typically all live inside of a roster directory. But to get
started we will execute a single roster file with Heist using the `salt.minion` Heist
manager:

.. code-block:: bash

    heist salt.minion -R roster.cfg

Assuming your roster is correct, heist will now connect to the remote
system and deploy the `salt.minion` binary.


Tear Down
=========

Heist is able to automatically clean up as well! Just soft kill
your heist application and it will reach out to all connections, tell them to
remove the deployed artifacts from the target systems and stop the service!
Like a proper heist these should be no evidence left behind!


Using Heist to Deploy Salt Minions
==================================
If you want to use Heist to deploy and manage Salt, you will need to install
`heist-salt <https://heist-salt.readthedocs.io/en/latest/>`_.


Additional Documentation
========================
If you want to read more about how to use Heist and its internals please
take a look at Heist's documentation here: https://heist.readthedocs.io/en/latest/
