================
Heist Subsystems
================

Since Heist is built on POP, a Python-based implementation of Plugin Oriented
Programming (POP), it is composed of several plugin subsystems, or ``subs``.
Each ``sub`` is loaded using POP's dynamic names interface and therefore can be
extended using vertical app-merging or adding additional plugins directly to
Heist.

Heist Subsystem
===============

The Heist subsystem is used to create managers for specific daemons. Therefore
if there is another agent that someone wanted to add to Heist to make it
disolvable and distributable via Heist they would add a plugin to the Heist
subsystem.

The required functions to add a new managed agent to Heist are:

run
----

This is the entry function. The ``run`` function is used to start the process
of creating tunnels and sending daemon code to target systems.

deploy
------

The ``deploy`` function is used to deploy the desired code down to the target
systems.

update
------

The ``update`` function is used to send an updated version of the desired code
down to the target system.

clean
-----

The ``clean`` function is called when Heist gets shut down. This is used to send
commands to the remote systems to shut down and clean up the agents.

Roster Subsystem
================

The roster subsystem is used to add ways to load up target system data. If
it is desired to load roster data from an alternative source a roster
can be easily added.

Rosters are very simple. They just need a single async function:

read
----

The ``read`` function is called to read in the roster data and returns the roster
data structure. The roster data structure is a python dict following this
structure:

.. code-block:: yaml

    hostname/id:
      logincred: data
      logindata: data
    hostname/id:
      logincred: data
      logindata: data

Artifact Subsystem
==================

The artifact system allows for code artifacts that will be deployed to target
systems to be downloaded from an artifact source. The artifact source will be
specific to the code that is being deployed. It is typical that an artifact
plugin will be built in concert with a specific Heist plugin.

get_version
-----------

Gather the available version data for the artifacts

get_artifact
------------

Download the actual artifact and store it locally so it can be sent down with
the Heist subsystem.

Tunnel Subsystem
================

The tunnel subsystem is used to establish communication tunnels with target
systems. If you want to use a system for tunneling other than SSH, or you want
to use a different SSH backend, just make a new tunnel plugin! The tunnel plugin
needs to be able to connect to remote systems, make network tunnels, copy files
and execute commands.

create
------

Used to create the new tunnel instance on the hub. This is where the persistent
connection or re-connection (if needed) logic is created.

send
----

The ability to send files to the target system is implemented here.

get
----

The ability to retrieve files from the target system is set up here.

cmd
----

This function runs shell commands on the target system

tunnel
------

This function creates a network tunnel from ports on the target system back to
ports on the source system

destroy
-------

Properly destroy a connection.
