async def run(hub, target_name: str):
    hub.log.info(
        "This is a test heist manager. You have installed heist correctly. "
        "Install a heist manager to use full functionality"
    )
    return {
        "comment": "This is a comment",
        "result": "Success",
        "retvalue": 0,
        "target": target_name,
    }


async def clean(hub, target_name: str):
    ...


def raw_run_cmd(hub, target_name: str):
    ...


def service_name(hub, target_name: str):
    ...


async def prepare_target(hub, target_name: str):
    return True


async def deploy(hub, target_name: str, **kwargs) -> str:
    target = hub.heist.TARGETS[target_name]
    return target.binary
