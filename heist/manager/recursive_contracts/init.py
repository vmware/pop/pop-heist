def sig_service_name(hub, target_name: str):
    """
    Get the name of the service that is associated with the deployed binary based on configuration in TARGETS

    target = hub.heist.TARGETS[target_name]
    """


async def sig_prepare_target(hub, target_name: str):
    """
    Modify the TARGETS structure with options unique to the manager

    target = hub.heist.TARGETS[target_name]
    """


def sig_raw_run_cmd(hub, target_name: str):
    """
    Get the command that can be used to run the artifact binary on the target based on configuration in TARGETS

    target = hub.heist.TARGETS[target_name]
    """


async def _sig_post_deploy(hub, target_name: str):
    """
    Perform any operations on the target that should happen after the artifact is deployed
    """
    # TODO this is where "generate aliases" and "get_grains" occurs


async def sig_run(hub, target_name: str):
    """
    Run the manager for a single target

    target = hub.heist.TARGETS[target_name]
    """


async def _sig_post_run(hub, target_name: str):
    """
    Perform any operations on the target that should happen after the manager is running on the target
    """
    # TODO this is where tunnels back to a salt master are created


async def _sig_manage(hub, target_name: str):
    """
    Perform any operations that need to be run regularly as the target is running
    """
    # TODO this is where "manage tunnel" happens


async def sig_clean(hub, target_name: str):
    """
    Perform further cleanup operations unique to the manager

    target = hub.heist.TARGETS[target_name]
    """


async def sig_deploy(hub, target_name: str, **kwargs) -> str:
    """
    Deploy the artifact for this manager

    target = hub.heist.TARGETS[target_name]
    """
