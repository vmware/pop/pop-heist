async def create(hub, name: str, **kwargs) -> bool:
    tunnel_plugin = hub.heist.TARGETS[name].tunnel_plugin
    reconnect = hub.heist.TARGETS[name].get("reconnect", False)
    if tunnel_plugin not in hub.heist.CONS:
        hub.heist.CONS[tunnel_plugin] = {}

    if name in hub.heist.CONS[tunnel_plugin] and not reconnect:
        hub.log.debug(f"{tunnel_plugin} tunnel for '{name}' already exists")
        return True

    return await hub.tunnel[tunnel_plugin].create(name, **kwargs)


async def send(hub, name: str, source: str, dest: str, **kwargs):
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].send(name, source, dest, **kwargs)


async def get(hub, name: str, source: str, dest: str):
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].get(name, source, dest)


async def cmd(hub, name: str, command: str, **kwargs):
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].cmd(name, command, **kwargs)


async def tunnel(hub, name: str, remote: str, local: str):
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].tunnel(name, remote, local)


async def destroy(hub, name: str):
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].destroy(name)


async def connected(hub, name: str) -> bool:
    tunnel_plugin = hub.heist.TARGETS[name]["tunnel_plugin"]
    return await hub.tunnel[tunnel_plugin].connected(name)
