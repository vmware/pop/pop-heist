from typing import Any

__virtualname__ = "asyncssh"


def __init__(hub):
    """
    Set up the objects to hold connection instances
    """
    hub.heist.CONS[__virtualname__] = {}
    hub.tunnel.asyncssh.TUNNEL_OPTIONS = set(
        hub.lib.inspect.getfullargspec(
            hub.lib.asyncssh.SSHClientConnectionOptions.prepare
        ).args
    )
    # Remove options from `inspect` that don't belong
    hub.tunnel.asyncssh.TUNNEL_OPTIONS -= {"self", "args", "kwargs", "tunnel"}
    # Add connection options that aren't specified in `SSHClientConnectionOptions.prepare`
    hub.tunnel.asyncssh.TUNNEL_OPTIONS.update(
        {"port", "loop", "family", "flags", "local_addr", "options"}
    )


def _get_asyncssh_opt(hub, target, option: str, default: Any = None) -> Any:
    """
    Get an assyncssh option from the target/roster
    :param option:
    :param target:
    :return:
    """
    result = target.get(option)
    if not result and "heist" in hub.OPT:
        result = hub.OPT.heist.get(option)
        if not result:
            result = hub.OPT.heist.roster_defaults.get(option)
    if not result:
        result = default
    return result


async def create(hub, name: str) -> bool:
    """
    Create a connection to the remote system using a dict of values that map
    to this plugin. Name the connection for future use, the connection data
    will be stored on the hub under hub.heist.CONS
    :param name:
    :param target:
    """
    target = hub.heist.TARGETS[name]
    roster = hub.heist.ROSTERS.get(name, {})
    id_ = target["id"]

    # Check for each possible SSHClientConnectionOption in the target, config, then autodetect (if necessary)
    con_opts = {"known_hosts": None}
    for arg in hub.tunnel.asyncssh.TUNNEL_OPTIONS:
        topt = _get_asyncssh_opt(hub, target, arg)
        ropt = _get_asyncssh_opt(hub, roster, arg)
        opt = topt or ropt
        if opt is not None:
            con_opts[arg] = opt

    try:
        con_opts.pop("host", None)
        conn = await hub.lib.asyncssh.connect(id_, **con_opts)
    except Exception as e:
        hub.log.error(f"Failed to connect to '{id_}': {e.__class__.__name__}: {e}")
        return False
    sftp = await conn.start_sftp_client()

    hub.heist.CONS[__virtualname__][name] = {
        "con": conn,
        "sftp": sftp,
    }
    return True


async def send(hub, name: str, source: str, dest: str, preserve=False, recurse=False):
    """
    Take the file located at source and send it to the remote system

    :param name: The name of the asyncssh tunnel connection
    :param source: The full path to the source location.
    :param dest: The full path to the destination location.
    :param preserve: Preserve the permissions on the destination host for
         the files being copied.
    :param recurse: Recursively copy a directory over to the destination
         host
    """

    conn = hub.heist.CONS[__virtualname__][name]
    target = hub.heist.TARGETS.get(name, {})
    sftp = conn["sftp"]
    if target.get("sudo"):
        # TODO run SFTP with permissions
        pass
    await sftp.put(source, dest, preserve=preserve, recurse=recurse)


async def get(hub, name: str, source: str, dest: str):
    """
    Take the file located on the remote system and copy it locally
    """
    sftp = hub.heist.CONS[__virtualname__][name]["sftp"]
    await sftp.get(source, dest)


async def cmd(hub, name: str, command: str, background=False, **kwargs):
    """
    Execute the given command on the machine associated with the named connection
    """
    target_os = hub.heist.TARGETS[name].get("os")
    sudo = hub.heist.TARGETS[name].get("sudo")
    sudo_password = hub.lib.re.compile(r"(?:.*)[Pp]assword(?: for .*)?:", hub.lib.re.M)
    con: hub.lib.asyncssh.SSHClientConnection = hub.heist.CONS[__virtualname__][name][
        "con"
    ]
    win_background_cmd = None
    if target_os == "windows":
        win_background_cmd = command
        arg_list = win_background_cmd.split()
        win_background_cmd = arg_list.pop(0)
        arg_list = " ".join(arg_list)
        win_background_cmd = (
            "powershell -command "
            + '"'
            + "; ".join(
                [
                    f"$action = New-ScheduledTaskAction -Execute '{win_background_cmd}' "
                    f"-Argument '{arg_list}'",
                    "$trigger = New-ScheduledTaskTrigger -Once -At 00:00",
                    "$principal = New-ScheduledTaskPrincipal "
                    '-UserId "$env:USERNAME" '
                    '-LogonType "S4U" '
                    "-RunLevel Highest",
                    "Register-ScheduledTask -Action $action "
                    "-Trigger $trigger "
                    "-Principal $principal "
                    "-TaskName heist-background "
                    "-Description 'Run background process' "
                    "-Force",
                    "Start-ScheduledTask -TaskName heist-background",
                    "Unregister-ScheduledTask -TaskName heist-background "
                    "-Confirm:$false",
                ]
            )
            + '"'
        )

    result = {}
    if hub.heist.CONS[__virtualname__][name].get("tty"):
        kwargs["term_type"] = (
            hub.heist.CONS[__virtualname__][name].get("term_type") or "xterm-color"
        )
        kwargs["term_size"] = hub.heist.CONS[__virtualname__][name].get(
            "term_size"
        ) or (
            80,
            24,
        )
    if sudo:
        if background:
            if win_background_cmd:
                command = win_background_cmd
            else:
                command = f"sudo -b su -c '{command} &'"
        else:
            if target_os != "windows":
                command = f"sudo {command}"
        async with con.create_process(command, **kwargs) as process:
            while not process.stdout.at_eof():
                output = await process.stdout.read(1024)
                if sudo_password.search(output):
                    process.stdin.write(
                        hub.heist.CONS[__virtualname__][name].get("password") + "\n"
                    )
                stdout = output
                if not stdout:
                    stdout = await process.stdout.read()
                stderr = await process.stderr.readline()
                result = hub.lib.dict.NamespaceDict(
                    stdout=stdout,
                    stderr=stderr,
                    returncode=process.returncode,
                    exit_status=process.returncode,
                )
    else:
        if background:
            if target_os == "windows":
                command = win_background_cmd
            else:
                command = f"{command} > /dev/null 2>&1 &"
        result = await con.run(command, **kwargs)
    return result


async def tunnel(hub, name: str, remote: str, local: str):
    """
    Given the local and remote addrs create a tcp tunnel through the connection
    """
    con = hub.heist.CONS[__virtualname__][name]["con"]
    await con.forward_remote_port("", remote, "localhost", local)


async def destroy(hub, name: str):
    """
    Destroy the named connection
    """
    if "con" in hub.heist.CONS[__virtualname__][name]:
        con = hub.heist.CONS[__virtualname__][name]["con"]
        if isinstance(con, hub.lib.asyncssh.SSHClientConnection):
            con.close()
            await con.wait_closed()


async def connected(hub, name: str) -> bool:
    """
    Determine whether the connection is
    connected or disconnected.
    """
    return bool(hub.heist.CONS[__virtualname__][name]["con"]._transport)
