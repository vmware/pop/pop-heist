from typing import Any
from typing import Dict


async def read(hub, roster_target: str = None) -> Dict[str, Any]:
    """
    Read in the data from the configured rosters
    """
    ret = {}
    rend = hub.OPT.heist.renderer

    if roster_target and hub.lib.pathlib.Path(roster_target).is_file():
        return await hub.rend.init.parse(roster_target, rend)
    else:
        hub.log.error(f"Missing roster file: {roster_target}")

    if hub.lib.pathlib.Path(str(hub.OPT.heist.roster_dir)).is_dir():
        for fn in hub.lib.os.listdir(hub.OPT.heist.roster_dir):
            full = hub.lib.os.path.join(hub.OPT.heist.roster_dir, fn)
            remotes = await hub.rend.init.parse(full, rend)
            if isinstance(remotes, dict):
                ret.update(remotes)
    return ret
