"""
Scan a netmask or ipaddr for open ssh ports
"""
from typing import Any
from typing import Dict


async def read(hub, roster_target: str = None) -> Dict[str, Any]:
    """
    read data from connection to specified ports
    to detect SSH ports
    """
    # TODO get target from "roster_target"
    ret = {}
    target = hub.OPT.heist.get("target")
    if not target:
        hub.log.critical("Need to define a target for the scan roster")
        return ret
    ports = hub.OPT.heist.ssh_scan_ports
    if not isinstance(ports, list):
        # Comma-separate list of integers
        ports = list(map(int, str(ports).split(",")))
    try:
        addrs = [hub.lib.ipaddress.ip_address(target)]
    except ValueError:
        try:
            addrs = hub.lib.ipaddress.ip_network(hub.OPT.heist.target).hosts()
        except ValueError:
            pass

    for addr in addrs:
        for port in ports:
            hub.log.debug(f"Scanning host: {addr} on port: {port}")
            try:
                if addr.version == 4:
                    fam = hub.lib.socket.AF_INET
                elif addr.version == 6:
                    fam = hub.lib.socket.AF_INET6
                else:
                    continue

                addr = addr.exploded
                reader, writer = await hub.lib.asyncio.open_connection(
                    addr, port, family=fam
                )
                data = await reader.read(100)
                if "ssh" not in data.decode().lower():
                    hub.log.critical(
                        f"Connection successful to {addr} "
                        f"but SSH information not returned."
                        f"Port {port} might not be an SSH port."
                    )
                ret[addr] = {"host": addr, "port": port}
                writer.close()
                if hub.lib.sys.version_info >= (3, 7):
                    await writer.wait_closed()
            except ConnectionRefusedError:
                hub.log.critical(f"Not able to connect to host: {addr} on port: {port}")
    return ret
