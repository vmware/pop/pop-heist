import sys

import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="heist")
    sys.exit(hub.heist.init.cli())
