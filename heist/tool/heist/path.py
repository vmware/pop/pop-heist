"""
Path operations for heist
"""
from typing import List


def remote(
    hub, target_os: str = "linux", entry_path: str = "", ext_path: List[str] = None
):
    """
    Convert local path to target (remote) path.

    Due to pathlib.Path docstring indicating that a PureWindowsPath and
    PosixPath can not be created on their counterpart's system, we must
    make our own conversion.
    :param target_os:
    :param entry_path:
    :param list ext_path:
    :return:
    """
    entry_path = entry_path
    ext_path = ext_path or []
    path_obj = None
    if "win" in target_os:
        path_obj = hub.lib.pathlib.PureWindowsPath(entry_path)
        for option in ext_path:
            path_obj = path_obj.joinpath(option)
    elif "linux" in target_os or "darwin" in target_os:
        path_obj = hub.lib.pathlib.PurePosixPath(entry_path)
        for option in ext_path:
            path_obj = path_obj.joinpath(option)
    hub.log.debug(f"Converted path for {target_os} for {entry_path}")
    return path_obj


def clean(hub, root: str = "/", path: str = "", subdir: bool = False) -> str:
    """
    Return a clean path that has been validated.
    Using os.path here instead of pathlib, because
    the api's functionalities are not entirely the
    same yet.
    """
    root = root or "/"
    path = path or ""
    real_root = hub.tool.heist.path.real(root)
    path = hub.lib.os.path.expandvars(path)
    if not hub.lib.os.path.isabs(real_root):
        return ""
    if not hub.lib.os.path.isabs(path):
        path = hub.lib.os.path.join(root, path)
    path = hub.lib.os.path.normpath(path)
    real_path = hub.tool.heist.path.real(path)
    if path.startswith(real_root):
        return hub.lib.os.path.join(root, path)
    if subdir:
        if real_path.startswith(real_root):
            return real_path
    else:
        if hub.lib.os.path.dirname(real_path) == hub.lib.os.path.normpath(real_root):
            return real_path
    return ""


def real(hub, path: str) -> str:
    """
    Cross-platform realpath method. On Windows when python 3, this method
    uses the os.readlink method to resolve any filesystem links.
    All other platforms and version use ``os.path.realpath``.
    """
    return hub.lib.os.path.realpath(path)
