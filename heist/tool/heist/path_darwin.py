__virtualname__ = "path"


def __virtual__(hub):
    if "lib" not in hub:
        return False, "'lib' has not been loaded yet"
    return hub.lib.sys.platform == "darwin", "This module only loads on darwin"


def real(hub, path) -> str:
    base = ""
    for part in path.parts[1:]:
        if base != "":
            if hub.lib.os.path.islink(hub.lib.os.path.sep.join([base, part])):
                base = hub.lib.os.readlink(hub.lib.os.path.sep.join([base, part]))
            else:
                base = hub.lib.os.path.abspath(hub.lib.os.path.sep.join([base, part]))
        else:
            base = hub.lib.os.path.abspath(hub.lib.os.path.sep.join([base, part]))
    return base
