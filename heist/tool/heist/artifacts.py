def get_artifact_dir(hub, target_os: str = "linux"):
    """
    function to get the full path to artifacts directory
    with the pkg_type included
    """
    artifacts_dir = hub.lib.pathlib.Path(
        hub.OPT.heist.artifacts_dir, "onedir", target_os
    )
    # TODO this is a remote path, this is not how to ensure a path exists on remote
    # if not artifacts_dir.is_dir():
    #     artifacts_dir.mkdir(parents=True)
    return str(artifacts_dir)


def get_artifact_suffix(hub, target_os: str = "linux"):
    """
    return the suffix for an artifact for
    each targeted OS.
    """
    suffix = "tar.xz"
    if target_os == "windows":
        suffix = ".zip"
    return suffix
