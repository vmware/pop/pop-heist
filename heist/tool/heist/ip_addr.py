def is_loopback(hub, addr: str) -> bool:
    """
    helper function to determine if an addr
    or hostname is a loopback address
    """
    try:
        info = hub.lib.socket.getaddrinfo(addr, 0)
    except hub.lib.socket.gaierror:
        hub.log.critical("Could not determine if addr is loopback")
        return False
    return hub.lib.ipaddress.ip_address(info[0][-1][0]).is_loopback
