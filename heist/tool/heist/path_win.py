__virtualname__ = "path"


def __virtual__(hub):
    if "lib" not in hub:
        return False, "'lib' has not been loaded yet"
    return hub.lib.sys.platform == "win32", "This module only loads on windows"


def real(hub, path) -> str:
    base = ""
    if not isinstance(path, hub.lib.pathlib.Path):
        path = hub.lib.pathlib.Path(path)
    for part in path.parts:
        if base != "":
            try:
                part = hub.lib.os.readlink(hub.lib.os.path.sep.join([base, part]))
                base = hub.lib.os.path.abspath(part)
            except OSError:
                base = hub.lib.os.path.abspath(hub.lib.os.path.sep.join([base, part]))
        else:
            base = part
    return base
