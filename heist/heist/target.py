def create(
    hub,
    name: str,
    *,
    manager: str = None,
    artifact_version: str = None,
    clean: bool = False,
    **opts,
) -> bool:
    """
    Sanitize and get defaults for the target machine
    """
    remote = hub.heist.ROSTERS.get(name, {})

    if name in hub.heist.TARGETS:
        hub.log.debug(f"Heist target already exists: {name}")
        return True

    if remote is None:
        remote = {}
    service_plugin = remote.get("service_plugin", opts.get("service_plugin"))
    manage_service = remote.get("manage_service", opts.get("manage_service"))
    tunnel_plugin = remote.get("tunnel_plugin", opts.get("tunnel_plugin", "asyncssh"))
    block = remote.get("block", opts.get("block", True))
    repo_data = opts.get("repo_data", {})

    if manager is None:
        manager = remote.get("manager", opts.get("manager"))

    # Create a path on host system to cache downloads
    artifacts_dir = remote.get("artifacts_dir", opts.get("artifacts_dir"))
    scripts_dir = None
    if artifacts_dir:
        artifacts_dir = hub.lib.pathlib.Path(artifacts_dir)
        artifacts_dir.mkdir(0o600, parents=True, exist_ok=True)
        scripts_dir = artifacts_dir / "scripts"

    if artifact_version:
        check_ver = artifact_version
        if "-" in artifact_version:
            check_ver, pkg_ver = artifact_version.split("-")
            if not isinstance(int(pkg_ver), int):
                raise ValueError(
                    f"Package version cannot be cast to an integer: {pkg_ver}"
                )
        valid_version = hub.lib.pkg_resources.safe_version(check_ver)

        if check_ver != valid_version:
            raise ValueError(f"version {check_ver} is not valid")

    # The id MUST be in the target, everything else might be in the target, conf, or elsewhere
    target_id = opts.pop("host", None) or opts.get("id") or name
    reconnect = remote.get("reconnect")

    target = hub.lib.dict.NamespaceDict(
        artifact_version=artifact_version,
        artifact_name=opts.get("artifact_name"),
        artifacts_dir=artifacts_dir,
        block=block,
        bootstrap=remote.get("bootstrap"),
        clean=clean,
        id=target_id,
        manage_service=manage_service,
        manager=manager,
        name=name,
        opts=opts,
        remote=remote,
        reconnect=reconnect,
        repo_data=repo_data,
        run_cmd=None,
        service_plugin=service_plugin,
        tunnel_plugin=tunnel_plugin,
        # The run_dir and binary are determined after deploying an artifact
        # TODO for now that happens in the heist plugin, after phase 2 it will be in heist-core
        binary=None,
        run_dir=None,
        service_name=None,
        scripts_dir=scripts_dir,
        # These will be determined in the next section
        kernel=None,
        os=None,
        os_path=None,
        run_dir_root=None,
        username=None,
        # Connection options
        sudo=remote.get("sudo", False),
        tty=remote.get("tty"),
        term_type=remote.get("term_type"),
        term_size=remote.get("term_size"),
        password=remote.get("password"),
        port=remote.get("port"),
    )
    # Initialize the base target attributes so that we can run a command for the rest
    hub.heist.TARGETS[name] = target

    return True


async def connect(hub, name: str, **opts) -> bool:
    """
    Connect to the target and update it's known values in the TARGETS dictionary
    """
    target = hub.heist.TARGETS[name]
    remote = hub.heist.ROSTERS.get(name, {})
    manager = target.manager

    hub.log.debug(f'Connecting to host: {target["id"]}')
    created = await hub.tunnel.init.create(name)
    if not created:
        hub.log.error(f'Connection to host {target["id"]} failed')
        return False

    hub.log.info(f'Connection to host {target["id"]} success')
    if not await hub.tunnel.init.create(name):
        return False

    hub.log.debug("Detecting target os and arch")
    target_os, kernel = await hub.heist.system.os_arch(name)
    target_os = target_os or "linux"
    hub.log.debug(f"Found target_os: {target_os}")

    run_dir_root = opts.get("run_dir_root")
    if not run_dir_root:
        run_dir_root = hub.heist.system.get_default("run_dir_root", target_os=target_os)

    # Get remote user
    user = remote.get("username") or opts.get("user")
    if not user:
        user = hub.heist.system.get_default(
            "username", target_os=target_os, run_dir_root=run_dir_root
        )
    hub.log.debug(f"Using remote user: {user}")

    run_dir = hub.tool.heist.path.remote(
        target_os,
        run_dir_root,
        ([f"heist_{user}", f"{hub.lib.secrets.token_hex()[:4]}"]),
    )

    os_path = opts.get("os_path")
    if not os_path:
        os_path = hub.heist.system.get_default(
            "os_path", target_os=target_os, run_dir_root=run_dir_root
        )

    remote_artifacts_dir = hub.tool.heist.artifacts.get_artifact_dir(
        target_os=target_os
    )

    hub.heist.TARGETS[name].update(
        dict(
            kernel=kernel,
            os=target_os,
            remote_artifacts_dir=remote_artifacts_dir,
            run_dir_root=run_dir_root,
            run_dir=run_dir,
            os_path=os_path,
            username=user,
        )
    )

    try:
        service_name = hub.manager[manager].service_name(name)
    except:
        # TODO safe backup service name
        return False

    hub.heist.TARGETS[name]["service_name"] = service_name

    try:
        # TODO this gets the command for running the raw binary, this will be an expected part of manager plugin
        run_cmd = hub.manager[manager].raw_run_cmd(name)
    except:
        # TODO safe backup run_cmd
        return False
    hub.heist.TARGETS[name]["run_cmd"] = run_cmd

    return True
