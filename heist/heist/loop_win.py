from typing import Any
from typing import Dict
from typing import List

__virtualname__ = "loop"


def __virtual__(hub):
    if "win" in hub.lib.sys.platform:
        return (True,)
    else:
        return (False, "This module only loads on windows hosts")


def start(hub, manager: str, *, artifacts_dir: str, **opts) -> List[Dict[str, Any]]:
    """
    Start the async loop and get the process rolling for nt based systems
    """
    # Create a path on host system to cache downloads
    hub.lib.pathlib.Path(artifacts_dir).mkdir(0o600, parents=True, exist_ok=True)
    hub.lib.subprocess.run(["icacls", artifacts_dir, "inheritance:d"], check=True)
    hub.lib.subprocess.run(
        ["icacls", artifacts_dir, "/remove", "Users", "/T"], check=True
    )
    hub.lib.subprocess.run(
        ["icacls", artifacts_dir, "/GRANT:R", "Users:(R)"], check=True
    )

    hub.pop.loop.create()
    try:
        return hub.pop.Loop.run_until_complete(
            hub.heist.remotes.run_roster(manager, artifacts_dir=artifacts_dir, **opts)
        )
    except KeyboardInterrupt:
        hub.log.debug("Caught keyboard interrupt")
    finally:
        hub.pop.Loop.run_until_complete(hub.tool.heist.clean.run())
        hub.pop.Loop.close()
