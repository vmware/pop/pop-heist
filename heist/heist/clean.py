async def run(hub, signal: int = None):
    """
    Clean up the connections
    """
    if signal:
        hub.log.warning(f"Got signal {signal}! Cleaning up connections")
    coros = []
    # First clean up the remote systems
    if not hub.OPT.heist.clean:
        hub.log.info("`noclean` was set. Will not clean up the artifact")
    else:
        for target_name, r_vals in hub.heist.ROSTERS.items():
            if not r_vals.get("bootstrap"):
                tunnel_plugin = hub.heist.TARGETS[target_name]["tunnel_plugin"]
                vals = hub.heist.CONS[tunnel_plugin][target_name]
                manager = vals["manager"]
                coros.append(
                    hub.manager[manager].clean(
                        target_name,
                    )
                )
        await hub.lib.asyncio.gather(*coros)
    # Then shut down connections
    coros = []
    for tunnel_plugin, connections in hub.heist.CONS.items():
        for target_name in connections:
            coros.append(hub.tunnel.init.destroy(target_name))
    await hub.lib.asyncio.gather(*coros)
    if hub.lib.sys.version_info >= (3, 7):
        tasks = [
            t
            for t in hub.lib.asyncio.all_tasks()
            if t is not hub.lib.asyncio.current_task()
        ]
    else:
        tasks = [
            t
            for t in hub.lib.asyncio.Task.all_tasks()
            if t is not hub.lib.asyncio.Task.current_task()
        ]
    for task in tasks:
        task.cancel()
