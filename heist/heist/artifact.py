from typing import Tuple


async def extract(
    hub,
    target_name: str,
) -> bool:
    """
    Extract the binary on the target.
    Uses tar for linux and unzip for windows.

    .. versionchanged:: v6.3.0
    """
    target = hub.heist.TARGETS[target_name]
    run_dir = target.run_dir
    binary = target.binary
    target_os = target.os
    path = run_dir / hub.lib.pathlib.Path(binary).name

    if not hub.tool.heist.path.clean(run_dir, hub.lib.pathlib.Path(binary).name):
        hub.log.error(f"The run_dir directory {run_dir} is not a valid path.")
        return False

    if target_os == "linux":
        cmd = f"tar -xvf {path} --directory={run_dir}"
    elif target_os == "windows":
        cmd = f"$ProgressPreference = 'SilentlyContinue'; Expand-Archive -LiteralPath '{path}' -DestinationPath {run_dir}"
    else:
        hub.log.error(f"Unable to extract artifact on os '{target_os}'")
        return False
    ret = await hub.tunnel.init.cmd(target_name, cmd)
    if ret.returncode == 0:
        return True
    hub.log.error(f"Could not extract {path} on the target")
    return False


def verify(hub, location, hash_value, hash_type="sha256") -> bool:
    with open(location, "rb") as fp:
        file_hash = getattr(hub.lib.hashlib, hash_type)(fp.read()).hexdigest()
        if not file_hash == hash_value:
            return False
    return True


async def fetch(hub, url: str, download: bool = False, location: str = None) -> bool:
    """
    Fetch a url and return json. If downloading artifact
    return the download location.
    """
    async with hub.lib.aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                if download and location:
                    with open(location, "wb") as fn_:
                        fn_.write(await resp.read())
                    return True
                return await resp.json()
            hub.log.critical(
                f"Cannot query url {url}. Returncode {resp.status} returned"
            )
            return False


async def get(hub, target_name: str) -> bool:
    """
    Fetch a url return the download location.
    """
    target = hub.heist.TARGETS[target_name]
    artifacts_dir = target.artifacts_dir
    artifact_name = target.artifact_name

    # pre-get operations
    version = target.get("version")
    repo_data = target.get("repo_data")
    if version:
        check_ver = version
        if "-" in version:
            check_ver, pkg_ver = version.split("-")
            assert isinstance(int(pkg_ver), int)
        valid_version = hub.lib.pkg_resources.safe_version(check_ver)
        assert check_ver == valid_version, f"version {check_ver} is not valid"

    if repo_data:
        if version not in repo_data:
            raise ValueError(f"version: {version} was not found in repo_data")
    if artifacts_dir is None:
        artifacts_dir = hub.lib.pathlib.Path(hub.OPT.heist.artifacts_dir)

    async with hub.lib.aiohttp.ClientSession() as session:
        with hub.lib.tempfile.TemporaryDirectory() as tmpdirname:
            # Download and verify the designated Heist manager artifact
            tmp_artifact_location = await hub.artifact[artifact_name].get(
                version=version,
                repo_data=repo_data,
                session=session,
                tmpdirname=hub.lib.pathlib.Path(tmpdirname),
            )
            if not tmp_artifact_location:
                return False

            artifact_location = artifacts_dir / tmp_artifact_location.name
            if not hub.tool.heist.path.clean(artifacts_dir, tmp_artifact_location.name):
                hub.log.error(
                    f"The tmp artifact {tmp_artifact_location.name} is not in the correct directory"
                )
                return False

            # artifact is already downloaded we do not need to copy/check
            if tmp_artifact_location == artifact_location:
                return True

            hub.log.info(
                f"Copying the artifact {artifact_location.name} to {str(artifacts_dir)}"
            )
            hub.lib.shutil.move(tmp_artifact_location, artifact_location)

    # ensure artifact was downloaded
    if not any(str(version) in x for x in hub.lib.os.listdir(str(artifacts_dir))):
        hub.log.critical(
            f"Did not find the {version} artifact in {str(artifacts_dir)}."
            f" Untarring the artifact failed or did not include version"
        )
        return False
    return artifact_location


def version(hub):
    # TODO Determine which artifact to use, find the right plugin, and find out the target's version of the artifact
    ...


async def deploy(
    hub,
    target_name: str,
) -> Tuple[str, str, bool, str]:
    """
    Deploy the artifact
    """
    target = hub.heist.TARGETS[target_name]
    target_os = target.os
    run_dir = target.run_dir
    artifact_name = target.artifact_name
    manager = target.manager

    cmd = "\n".join(
        [
            s.strip()
            for s in f"""/bin/sh << 'EOF'
if [ -d {run_dir.parent} ]; then
    ls {run_dir.parent};
    exit 0
else
    echo "The {run_dir.parent} does not exist. Deploying artifact"
    exit 1
fi
EOF""".split(
                "\n"
            )
        ]
    )

    if target_os == "windows":
        cmd = "; ".join(
            [
                f"$Folder = '{run_dir.parent}'",
                "if (Test-Path -Path $Folder) {Get-ChildItem -name $Folder} else {exit 1}",
            ]
        )

    # check if there was a previous heist deployment
    use_prev = False
    prev_artifact = None
    prev = await hub.tunnel.init.cmd(target_name, cmd)
    ret = None
    if prev.returncode == 1:
        if manager in hub.manager:
            ret = await hub.manager[manager].deploy(
                target_name,
            )
    else:
        run_dir = run_dir.parent / prev.stdout.strip()
        get_binary = f"ls {run_dir}/"
        use_prev = True
        ret = await hub.manager[artifact_name].deploy(
            target_name,
            verify=True,
        )
        artifact_files = await hub.tunnel.init.cmd(target_name, get_binary)
        suffix = hub.tool.heist.artifacts.get_artifact_suffix(target_os=target_os)
        prev_artifact = [x for x in artifact_files.stdout.split() if suffix in x]
        if prev_artifact:
            prev_artifact = prev_artifact[0]
    return run_dir, ret, use_prev, prev_artifact


async def verify_checksum(
    hub,
    target_name: str,
    source_fp,
    target_fp,
    hash_type=None,
) -> bool:
    """
    Verify checksum file
    """
    target = hub.heist.TARGETS[target_name]
    target_os = target.os
    run_dir = target.run_dir
    if hash_type is None:
        hash_type = "sha512"

    # validating hash_type
    if not getattr(hub.lib.hashlib, hash_type):
        hub.log.error(f"hash_type of {hash_type} is not valid")
        return False

    # validate paths
    if not hub.tool.heist.path.clean(run_dir, target_fp):
        hub.log.error(f"Path is not valid: {target_fp}")
        return False
    if not hub.lib.pathlib.Path(source_fp).is_file():
        hub.log.error(f"Source path does not exist: {source_fp}")
        return False

    if target_os == "windows":
        cmd = "; ".join(
            [
                f"Get-ChildItem -Recurse {target_fp.parent} | Get-FileHash -Algorithm {hash_type.upper()} | Format-List"
            ]
        )
    else:
        cmd = f"cd {run_dir}; {hash_type}sum -c {target_fp}"
    ret = await hub.tunnel.init.cmd(target_name, cmd)
    if ret.returncode != 0:
        hub.log.error(f"Could not verify the checksum of the artifact")
        return False
    if target_os == "windows":
        target_data = {}
        for line in ret.stdout.split(" : "):
            line = line.strip()
            if line.startswith(("Algorithm", "SHA")):
                continue
            if "Path" in line:
                _hash = "".join(line.split()).replace("Path", "")
                continue
            if "Algorithm" in line:
                _path = line.split()[0]
            target_data[_hash] = _path
    else:
        cmd = f"cat {target_fp}"
        ret = await hub.tunnel.init.cmd(target_name, cmd)

    with open(source_fp) as fp:
        data = fp.read()
        if target_os == "windows":
            items = iter(data.split())
            source_data = dict(zip(items, items))
            test = {}
            for key, value in source_data.items():
                key = key.upper()
                getattr(hub.lib.hashlib, hash_type)().block_size
                check = r"([a-fA-F\d]){128}"
                is_hash = hub.lib.re.match(check, key)
                if is_hash:
                    _hash = key
                    _path = value
                else:
                    is_hash = hub.lib.re.match(check, value)
                    if is_hash:
                        _hash = value.upper()
                        _path = key

                test[_hash] = _path

                if not target_data.get(_hash):
                    hub.log.error(
                        f"Could not verify the checksum of the artifact for {_hash} and {_path}"
                    )
                    return False
                else:
                    target_data[_hash] == _path

        else:
            if data != ret.stdout:
                hub.log.error(f"Could not verify the checksum of the artifact")
                return False
    return ret


def checksum(hub, files, hash_type: str = None) -> dict:
    """
    Create checksum for files to be copied over
    for the artifact.
    """
    if not hash_type:
        hash_type = "sha512"

    # validating hash_type
    if not getattr(hub.lib.hashlib, hash_type):
        hub.log.error(f"hash_type of {hash_type} is not valid")
        return {}

    ret = {}
    for fp in files:
        fp = hub.lib.pathlib.Path(fp)
        if not fp.exists():
            hub.log.error(f"Could not create checksum. Path {fp} does not exist")
            return {}
        hash_obj = getattr(hub.lib.hashlib, hash_type)()
        count = 1
        if fp.is_file():
            hash_obj.update(open(fp, "rb").read())
            ret[hash_obj.hexdigest()] = fp
        if fp.is_dir():
            for _fp in fp.rglob("*"):
                hash_obj = getattr(hub.lib.hashlib, hash_type)()
                count += 1
                if _fp.is_file():
                    hash_obj.update(open(_fp, "rb").read())
                    ret[hash_obj.hexdigest()] = _fp
    return ret


async def clean(hub, target_name: str):
    """
    Clean up the deployed artifact and files
    """
    target = hub.heist.TARGETS[target_name]
    # remove run directory
    run_dir = target.run_dir
    target_os = target.os

    # clean alias symlinks
    if target_os == "linux":
        scripts_dir = run_dir / "scripts"
        cmd_alias = f"ls {scripts_dir}"
        get_files = await hub.tunnel.init.cmd(target_name, cmd_alias)
        if get_files.returncode != 0:
            hub.log.error("Did not find alias files. Not removing")
        else:
            for _file in get_files.stdout.split():
                _file = str(scripts_dir / _file)
                find_symlink = f"find -L /usr/bin/ -samefile {_file}"
                get_symlink = await hub.tunnel.init.cmd(target_name, find_symlink)
                if get_symlink.returncode != 0:
                    hub.log.error(f"Could not find the symlink to script {_file}")
                    break
                rm_symlink = f"rm {get_symlink.stdout.strip()}"
                rm_symlink = await hub.tunnel.init.cmd(target_name, rm_symlink)
                if rm_symlink.returncode != 0:
                    hub.log.error(f"Could not remove the symlink of file {_file}")
                    break

    if target_os == "windows":
        cmd_isdir = (
            f"If ( (Get-Item {run_dir}).PSIsContainer ) {{ exit 0 }} Else {{ exit 1 }}"
        )
        cmd_rmdir = f"cmd /c rmdir /s /q {run_dir}"
    else:
        cmd_isdir = f"[ -d {run_dir} ]"
        cmd_rmdir = f"rm -rf {run_dir}"

    # Make sure it is a directory
    ret = await hub.tunnel.init.cmd(target_name, cmd_isdir)
    if ret.returncode == 0:
        await hub.tunnel.init.cmd(target_name, cmd_rmdir)

    # remove parent directory if its empty
    # If it's not empty, there might be another running instance of heist that
    # was previously deployed
    await hub.tunnel.init.cmd(
        target_name,
        f"rmdir {hub.heist.TARGETS[target_name]['run_dir'].parent}",
    )


async def create_aliases(hub, target_name: str, content, aliases=None) -> bool:
    """
    Create the alias files that will be sent
    to the target
    """
    target = hub.heist.TARGETS[target_name]
    target_os = target.os
    artifacts_dir = target.scripts_dir
    artifacts_dir.mkdir(exist_ok=True)
    # aliases should follow the dictionary structure:
    # {"binary-name": {"file": "path_to_binary_to_copy", "cmd": "cmd used to run on target"},
    #  "binary-name2": {"file": "path_to_binary_to_copy", "cmd": "cmd used to run on target"}}
    for alias in aliases:
        alias_file = aliases[alias]["file"]
        alias_cmd = aliases[alias]["cmd"]
        if target_os == "windows":
            alias_file = alias_file.parent / f"{alias_file.name}.bat"
        # this requires {alias} to be in the content.
        alias_content = content.format(alias=alias_cmd)
        with open(alias_file, "w") as fp:
            fp.write(alias_content)
        alias_file.chmod(711)
    return True


async def deploy_aliases(hub, target_name: str, aliases_dir: str) -> bool:
    """
    Deploy the aliases files to the target
    and setup correct path environment variables
    """
    target = hub.heist.TARGETS[target_name]
    target_os = target.os
    artifacts_dir = target.artifacts_dir / "scripts"

    await hub.tunnel.init.send(
        target_name,
        artifacts_dir,
        target.run_dir,
        preserve=True,
        recurse=True,
    )
    if target_os == "windows":
        path_cmd = f'setx PATH "{aliases_dir};$env:PATH"'
        ret = await hub.tunnel.init.cmd(target_name, path_cmd)
        if ret.returncode != 0:
            hub.log.error(f"Could not add {artifacts_dir} to windows PATH environment")
            return False
    else:
        path_cmd = f"ln -s {aliases_dir}/* /usr/bin/"
        ret = await hub.tunnel.init.cmd(target_name, path_cmd)
        if ret.returncode != 0:
            hub.log.error(f"Could not add aliases to PATH")
            return False
    return True
