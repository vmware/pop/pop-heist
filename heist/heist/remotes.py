from typing import Any
from typing import Dict
from typing import List


async def run_roster(
    hub,
    manager: str,
    roster_file: str = "",
    roster: str = None,
    roster_data: Dict[str, Any] = None,
    **opts,
) -> List[Dict[str, Any]]:
    """
    Read a roster, then run heist on each target in the roster
    """
    remotes = await hub.roster.init.read(
        roster, roster_file=roster_file, roster_data=roster_data
    )

    if not remotes:
        raise ValueError("No remotes")

    return await hub.heist.remotes.run(manager=manager, remotes=remotes, **opts)


async def run(
    hub,
    manager: str,
    remotes: Dict[str, Any],
    **opts,
) -> List[Dict[str, Any]]:
    """
    Sanitize the running opts then run all remotes in a roster
    """
    hub.log.debug(f"Heist manager is '{manager}'")

    # Create asyncio tasks for running heist on each of the remotes
    coros = []
    for target_name, remote in remotes.items():
        coro = hub.heist.manager.run(
            target_name, manager=manager, remote=remote, **opts
        )
        coros.append(coro)

    # Run each of the remotes simultaneously and return each as they complete
    ret_data = []
    for coro in hub.lib.asyncio.as_completed(coros):
        try:
            ret = await coro
        except OSError as err:
            ret = {"result": "Error", "comment": f"OS error: {err}", "retvalue": 1}
        except ValueError:
            ret = {
                "result": "Error",
                "comment": "a Value of unknown type was encountered",
                "retvalue": 2,
            }
        hub.log.debug(f"Heist run return value: {ret}")
        if isinstance(ret, list):
            ret_data.extend(ret)
        else:
            ret_data.append(ret)

    return ret_data
