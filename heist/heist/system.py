from typing import Tuple


async def os_arch(hub, target_name: str) -> Tuple[str, str]:
    """
    Query the target system for the OS and architecture type
    """

    DELIM = "'|'"
    ret = await hub.tunnel.init.cmd(
        target_name,
        f'echo "$OSTYPE{DELIM}$MACHTYPE{DELIM}$env:PROCESSOR_ARCHITECTURE"'
        # target_name, f'echo "$OSTYPE|$MACHTYPE|$env:PROCESSOR_ARCHITECTURE"'
    )

    if ret.returncode:
        raise ChildProcessError(ret.stderr)

    kernel, arch, winarch = ret.stdout.lower().split("|", maxsplit=2)

    # Set the architecture bit
    if "64" in winarch or "64" in arch:
        os_arch_ = "amd64"
    else:
        os_arch_ = "i386"

    # Set the kernel bit
    if "linux" in kernel:
        kernel = "linux"
    elif "darwin" in kernel:
        kernel = "darwin"
    elif "bsd" in kernel:
        kernel = "bsd"
    elif winarch:
        kernel = "windows"
    else:
        raise ValueError(
            f"Could not determine arch from kernel: {kernel} arch: {arch} winarch: {winarch}"
        )
    hub.log.debug(f'Detected arch "{os_arch_}" on target')
    return kernel, os_arch_


def get_default(hub, key: str, target_os: str = "default", run_dir_root: str = None):
    """
    Get default values for the target OS
    """
    os_defaults = {
        "linux": {
            "os_path": "/var/tmp/heist",
            "user": "root",
            "run_dir_root": run_dir_root or "/var/tmp/",
        },
        "windows": {
            "os_path": "C:\\Windows\\temp\\heist",
            "user": "Administrator",
            "run_dir_root": run_dir_root or "C:\\Windows\\temp\\",
        },
        "default": {
            "os_path": "/var/tmp/heist",
            "user": "root",
            "run_dir_root": run_dir_root or "/var/tmp/",
        },
    }
    if target_os not in os_defaults:
        hub.log.error(f"OS '{target_os}' not defined in hub.heist.OS_DEFAULTS")

    defaults = os_defaults.get(target_os, os_defaults["default"])

    if key not in defaults:
        hub.log.error(f"No {target_os}-specific default for {key}, using fallback")

    return defaults.get(key, os_defaults["default"].get(key))
