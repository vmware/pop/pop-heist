"""
The entry point for Heist, this is where config loading and the project
spine is set up
"""


async def run(
    hub, target_name: str, *, manager: str = None, remote: dict = None, **opts
) -> dict:
    """
    Call heist the easy way, passing target name to a plugin with a giant run function that takes only "taget_name"
    TODO In the next refactor phase -- more will be done in this function and much less will be done in the plugins
    """
    # Store an immutable copy of the roster data
    hub.heist.ROSTERS[target_name] = hub.pop.data.imap(hub.lib.copy.copy(remote))

    # Initialize this remote in TARGETS
    success = hub.heist.target.create(
        target_name,
        manager=manager,
        **opts,
    )
    if not success:
        return {
            "comment": "Unable to initialize the target",
            "result": "Error",
            "retvalue": 1,
            "target": target_name,
        }
    target = hub.heist.TARGETS[target_name]

    # Initialize this remote in CONS
    success &= await hub.heist.target.connect(
        target_name,
        **opts,
    )
    if not success:
        return {
            "comment": f"Could not establish tunnel with {target.id}",
            "result": "Error",
            "retvalue": 1,
            "target": target_name,
        }

    hub.log.debug(f"Validating path: {target.run_dir}")
    if not hub.tool.heist.path.clean(target.run_dir_root, target.run_dir):
        return {
            "comment": f"The run_dir {target.run_dir} is not a valid path",
            "result": "Error",
            "retvalue": 1,
            "target": target_name,
        }

    # Allow the manager to make modifications to the TARGET
    success &= await hub.manager[manager].prepare_target(
        target_name,
    )
    # TODO verify that after prepare_target, options that can only come from the plugin exist
    # TODO also do that after the deploy phases

    if not success:
        return {
            "comment": "Cannot manage the service, previous artifact never deployed",
            "result": "Error",
            "retvalue": 1,
            "target": target_name,
        }

    ret = await hub.manager[manager].run(target_name)

    return ret
