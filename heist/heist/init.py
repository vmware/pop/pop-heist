"""
The entry point for Heist, this is where config loading and the project
spine is set up
"""


def __init__(hub):
    # Structures for managing connections, rosters, and targets
    hub.heist.CONS = {}
    hub.heist.ROSTERS = {}
    hub.heist.TARGETS = {}

    # Put all python imports used throughout heist onto a single location on the hub
    hub.pop.sub.add(dyne_name="lib")
    hub.pop.sub.add(python_import="aiohttp", sub=hub.lib)
    hub.pop.sub.add(python_import="asyncio", sub=hub.lib)
    hub.pop.sub.add(python_import="asyncssh", sub=hub.lib)
    hub.pop.sub.add(python_import="colorama", sub=hub.lib)
    hub.pop.sub.add(python_import="copy", sub=hub.lib)
    hub.pop.sub.add(python_import="dict_tools.data", subname="dict", sub=hub.lib)
    hub.pop.sub.add(python_import="hashlib", sub=hub.lib)
    hub.pop.sub.add(python_import="inspect", sub=hub.lib)
    hub.pop.sub.add(python_import="ipaddress", sub=hub.lib)
    hub.pop.sub.add(python_import="json", sub=hub.lib)
    hub.pop.sub.add(python_import="os", sub=hub.lib)
    hub.pop.sub.add(python_import="pathlib", sub=hub.lib)
    hub.pop.sub.add(python_import="pkg_resources", sub=hub.lib)
    hub.pop.sub.add(python_import="re", sub=hub.lib)
    hub.pop.sub.add(python_import="rend.exc", sub=hub.lib)
    hub.pop.sub.add(python_import="secrets", sub=hub.lib)
    hub.pop.sub.add(python_import="shutil", sub=hub.lib)
    hub.pop.sub.add(python_import="socket", sub=hub.lib)
    hub.pop.sub.add(python_import="subprocess", sub=hub.lib)
    hub.pop.sub.add(python_import="sys", sub=hub.lib)
    hub.pop.sub.add(python_import="tempfile", sub=hub.lib)
    hub.pop.sub.add(python_import="warnings", sub=hub.lib)

    # Load all relative subs onto the hub recursively
    for dyne in (
        "artifact",
        "rend",
        "roster",
        "service",
        "tunnel",
        "tool",
        "output",
        "manager",
    ):
        hub.pop.sub.add(dyne_name=dyne)
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.load_subdirs(hub.manager, recurse=True)

    # These are the plugins that will have their config loaded onto hub.OPT
    hub.heist.CONFIG_LOAD = ["heist", "rend"]


def cli(hub) -> int:
    """
    Load the cli as the first thing we do to account for pluggable OS dependent systems.
    """
    hub.pop.config.load(hub.heist.CONFIG_LOAD, cli="heist")

    if not hub.SUBPARSER:
        try:
            managers = [sub.__name__ for sub in hub.manager]
            hub.log.error(f"Heist must be run with a manager: {','.join(managers)}")
        except:
            ...
        print(hub.args.parser.help())
        return 2

    ret = hub.heist.loop.start(manager=hub.SUBPARSER, **hub.OPT.heist)
    outputter = hub.OPT.rend.output

    # Only print if we are calling heist from the cli
    print(hub.output[outputter].display(ret))
    return 0
