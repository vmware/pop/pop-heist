from typing import Any
from typing import Dict
from typing import List

__virtualname__ = "loop"


def start(hub, manager: str, **opts) -> List[Dict[str, Any]]:
    """
    Start the async loop and get the process rolling for *nix based systems
    """
    try:
        ret = hub.pop.loop.start(
            hub.heist.remotes.run_roster(manager, **opts),
            sigint=hub.heist.clean.run,
            sigterm=hub.heist.clean.run,
        )
        return ret[0]
    except hub.lib.asyncio.CancelledError:
        hub.log.debug("Cancelled remaining running asyncio tasks")
    finally:
        hub.pop.Loop.close()
