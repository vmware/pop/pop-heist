async def disable(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(target_name, f"systemctl disable {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def enable(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(target_name, f"systemctl enable {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def start(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    block = target.get("block", True)
    cmd = [f"systemctl start {service}"]
    if not block:
        cmd.append("--no-block")
    ret = await hub.tunnel.init.cmd(target_name, " ".join(cmd))
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def stop(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(target_name, f"systemctl stop {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def restart(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(target_name, f"systemctl restart {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


def conf_path(hub, target_name) -> str:
    target = hub.heist.TARGETS[target_name]
    service_name = target.service_name
    return hub.tool.heist.path.remote(
        "linux", "/etc", ["systemd", "system", f"{service_name}.service"]
    )


async def clean(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    await hub.tunnel.init.cmd(target_name, f"systemctl daemon-reload")


async def status(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    cmd = [f"systemctl status {service}"]
    ret = await hub.tunnel.init.cmd(target_name, " ".join(cmd))
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True
