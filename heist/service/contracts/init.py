def sig_disable(hub, target_name):
    ...


def sig_enable(hub, target_name):
    ...


def sig_start(hub, target_name):
    ...


def sig_stop(hub, target_name):
    ...


def sig_restart(hub, target_name):
    ...


def sig_clean(hub, target_name):
    ...


def sig_status(hub, target_name):
    ...
