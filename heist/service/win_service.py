async def disable(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Set-Service -Name {service} -StartupType Disabled; "
        f"If ($?) {{ exit 0 }} else {{ exit 1 }}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def enable(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Set-Service -Name {service} -StartupType Automatic; "
        f"If ($?) {{ exit 0 }} else {{ exit 1 }}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def start(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Start-Service -Name {service}; If ($?) {{ exit 0 }} else {{ exit 1 }}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def stop(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Stop-Service -Name {service}; If ($?) {{ exit 0 }} else {{ exit 1 }}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def restart(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Restart-Service -Name {service}; If ($?) {{ exit 0 }} else {{ exit 1 }}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


def conf_path(hub, service_name) -> str:
    # TODO this needs to be a function for the manager after 2nd phase of refactor
    pass


async def clean(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(target_name, f"cmd /c sc delete {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def status(hub, target_name):
    """
    Get status of a service on windows
    """
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    service = target.service_name
    ret = await hub.tunnel.init.cmd(
        target_name,
        f"Get-Service {service}",
    )
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True
