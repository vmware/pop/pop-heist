async def disable(hub, target_name) -> bool:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    await hub.service[service_plugin].disable(target_name)


async def enable(hub, target_name) -> bool:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    await hub.service[service_plugin].enable(target_name)


async def start(hub, target_name) -> bool:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    return await hub.service[service_plugin].start(target_name)


async def stop(hub, target_name) -> bool:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    return await hub.service[service_plugin].stop(
        target_name,
    )


async def restart(hub, target_name) -> bool:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    service = hub.heist.TARGETS[target_name].service_name
    hub.log.debug(f"Restarting the service {service}")
    return await hub.service[service_plugin].restart(target_name)


def service_conf_path(hub, target_name) -> str:
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    service_name = hub.heist.TARGETS[target_name].service_name
    conf_path = hub.service[service_plugin].conf_path(service_name)
    if not conf_path:
        return ""
    return conf_path


async def clean(
    hub,
    target_name,
) -> bool:
    target = hub.heist.TARGETS[target_name]
    service_plugin = target.service_plugin
    service_name = target.service_name
    target.os
    target.tunnel_plugin
    if not service_plugin:
        service_plugin = hub.heist.CONS[target_name]["service_plugin"]
    # stop service
    await hub.service[service_plugin].stop(target_name)

    service_conf = hub.service.init.service_conf_path(service_name)
    if service_conf:
        ret = await hub.tunnel.init.cmd(target_name, f"[ -f {service_conf} ]")
        if ret.returncode == 0:
            await hub.tunnel.init.cmd(target_name, f"rm -f {service_conf}")
        else:
            hub.log.error(
                f"The conf file {service_conf} does not exist. Will not attempt to remove"
            )

    await hub.service[service_plugin].clean(target_name, service_name)


async def status(hub, target_name) -> bool:
    """
    Check the status of the service and return if it's
    running or not.
    """
    service_plugin = hub.heist.TARGETS[target_name].service_plugin
    return await hub.service[service_plugin].status(target_name)


def conf_path(hub, target_name) -> str:
    # TODO this needs to be a function for the manager after 2nd phase of refactor
    pass
