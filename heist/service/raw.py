async def disable(hub, target_name) -> bool:
    pass


async def enable(hub, target_name) -> bool:
    pass


async def start(
    hub,
    target_name,
) -> bool:
    """
    Start the service in the background
    """
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    target.os
    service = target.service_name
    target.run_dir
    run_cmd = target.run_cmd
    # TODO each heist manager will need to have a function that validates the run_cmd input
    # if not hub.tool.heist.service.valid_run_cmd(run_cmd, run_dir):
    #    hub.log.error(
    #        f"The run command is not valid. Not starting the service {service}"
    #    )
    #    return False
    cmd = [f"{run_cmd}"]
    hub.log.info(f"Starting the service {service}")
    ret = await hub.tunnel.init.cmd(target_name, " ".join(cmd), background=True)
    if ret.returncode != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def stop(hub, target_name) -> bool:
    """
    Stop the service
    """
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    target_os = target.os
    service = target.service_name
    if target_os == "windows":
        kill_cmd = (
            f'powershell -command "Stop-Process -Name {service} -ErrorAction SilentlyContinue; '
            'If ($?) { exit 0 } else { exit 1 }"'
        )
    else:
        kill_cmd = f"pkill -f {service}"
    hub.log.debug(f"Attempting to kill {service} with: {kill_cmd}")
    ret = await hub.tunnel.init.cmd(target_name, kill_cmd)
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    hub.log.info(f"Successfully killed {service}")
    return True


async def restart(
    hub,
    target_name,
) -> bool:
    target = hub.heist.TARGETS[target_name]
    service = target.service_name
    if await hub.service.raw.status(target_name):
        if not await hub.service.raw.stop(target_name):
            hub.log.error(f"Could not stop the service {service}")
            return False
        await hub.lib.asyncio.sleep(2)
    while not hub.service.raw.status(target_name):
        await hub.lib.asyncio.sleep(5)
        continue
    if not await hub.service.raw.start(target_name):
        hub.log.error(f"Could not start the service {service}")
        return False
    return True


def conf_path(hub, target_name) -> str:
    pass


async def clean(hub, target_name) -> bool:
    pass


async def status(hub, target_name) -> bool:
    target = hub.heist.TARGETS[target_name]
    target.tunnel_plugin
    target_os = target.os
    service = target.service_name
    cmd = [f"pgrep -f {service}"]
    if target_os == "windows":
        cmd = [f"get-process {service}"]
    ret = await hub.tunnel.init.cmd(target_name, " ".join(cmd))
    if ret.returncode != 0:
        hub.log.info(f"The service {service} is not running")
        return False
    hub.log.info(f"The service {service} is running")
    return True
