import asyncio
import itertools
import os
import subprocess
import sys
import tempfile
import time
import uuid
from typing import Generator
from typing import Set
from typing import Tuple

import psutil
import pytest

try:
    import tests.helpers.sftp_server as sftp_server

    HAS_SFTP = True
except (ImportError, ModuleNotFoundError):
    HAS_SFTP = False


def used_ports() -> Set[int]:
    """
    :return: A set of all the used ports on the local system
    """
    return {x.laddr.port for x in psutil.net_connections()}


def unused_ports() -> Generator[int, None, None]:
    """
    :return: A generator that gets the next unused port from the user range
    """
    for port in itertools.cycle(range(49152, 65535)):
        if port not in used_ports():
            yield port


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
def ssh_keypair() -> Tuple[str, str]:
    """
    :return: The absolute paths of static ssh keys in the helpers directory
    """
    helpers_dir = os.path.dirname(sftp_server.__file__)
    return (
        os.path.join(helpers_dir, "id_rsa_testing"),
        os.path.join(helpers_dir, "id_rsa_testing.pub"),
    )


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
def spawn_server(sftp_port: int, pid_file: str, **kwargs) -> subprocess.Popen:
    """
    :param sftp_port: The port for the server to run on
    :param pid_file: The path where the server will create a PID file
    :param kwargs: SSHServer connection options that will be passed to the server
    :return: A subprocess Pipe to the process
    """
    server_cmd = [
        sys.executable,
        sftp_server.__file__,
        f"--sftp-port={sftp_port}",
        f"--pid-file={pid_file}",
    ] + [f'--{key.replace("_", "-")}={value}' for key, value in kwargs.items()]
    print(" ".join(server_cmd))
    process = subprocess.Popen(
        server_cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE
    )

    # Allow the server time to start up
    for _ in range(1000):
        if os.path.exists(pid_file) or (process.poll() is not None):
            # At this point either the server is running or crashed because of the args you gave it
            break
        else:
            time.sleep(0.1)

    # Verify that it started properly
    assert not process.poll(), b"\n".join(process.stderr.readlines())
    return process


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
def random_file(directory: str = None, name: str = None, size: int = 1024) -> str:
    """
    :param name: The text name of the file, defaults to a random string
    :param directory: The directory to create the file in; defaults to /tmp or it's equivalent
    :param size: The amount of random bytes to fill the file with
    :return:
    """
    if not directory:
        directory = tempfile.gettempdir()
    if not name:
        name = str(uuid.uuid4())[:10]
    path = os.path.join(directory, name)
    with open(path, "wb+") as out:
        out.write(os.urandom(size))
    return path


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
def sftp_root() -> tempfile.TemporaryDirectory:
    temp_dir = tempfile.TemporaryDirectory(
        prefix="heist_asyncssh_", suffix="_root_data"
    )
    return temp_dir


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.fixture(scope="function")
def temp_dir() -> tempfile.TemporaryDirectory:
    temp_dir = tempfile.TemporaryDirectory(
        prefix="heist_asyncssh_", suffix="_test_data"
    )
    yield temp_dir.name


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.fixture(scope="function")
def basic_sftp_server(hub) -> Tuple[int, str, str]:
    # Generate a unique pid_file name, but do not create the file
    pid_file = os.path.join(
        tempfile.gettempdir(), f"asyncssh_test_{str(uuid.uuid4())[:8]}.pid"
    )
    private_key, public_key = ssh_keypair()
    root = sftp_root()
    port = unused_ports()
    sftp_port = next(port)
    server = spawn_server(
        sftp_port=sftp_port,
        authorized_client_keys=public_key,
        server_host_keys=private_key,
        pid_file=pid_file,
        sftp_root=root.name,
    )
    yield sftp_port, private_key, root.name
    server.kill()
    if os.path.exists(pid_file):
        os.remove(pid_file)
    hub.heist.CONS["asyncssh"] = {}


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.fixture(scope="function")
def sftp_ssh_server(hub) -> Tuple[int, int, str]:
    # Generate a unique pid_file name, but do not create the file
    pid_file = os.path.join(
        tempfile.gettempdir(), f"asyncssh_test_{str(uuid.uuid4())[:8]}.pid"
    )
    private_key, public_key = ssh_keypair()
    port = unused_ports()
    sftp_port = next(port)
    ssh_port = next(port)
    # TODO for testing commands should ssh and sftp be on the same port?
    # If yes then set `reuse_address` and `reuse_port` to `True`
    server = spawn_server(
        sftp_port=sftp_port,
        ssh_port=ssh_port,
        authorized_client_keys=public_key,
        server_host_keys=private_key,
        pid_file=pid_file,
    )
    yield sftp_port, ssh_port, private_key
    server.kill()
    if os.path.exists(pid_file):
        os.remove(pid_file)
    hub.heist.CONS["asyncssh"] = {}


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.mark.asyncio
async def test_create_and_destroy(
    hub, tmp_path, basic_sftp_server: Tuple[int, str, str], target_name
):
    # Setup
    port, private_key, _ = basic_sftp_server
    hub.heist.target.create(
        target_name,
        manager="test",
        artifacts_dir=tmp_path,
        artifact_version="",
        clean=True,
        remote={},
        host=target_name,
        port=port,
        known_hosts=None,
        client_keys=[private_key],
    )
    assert hub.heist.TARGETS[target_name]
    await hub.heist.target.connect(
        target_name,
    )

    # Execute
    if not await hub.tunnel.asyncssh.create(name=target_name):
        raise pytest.skip(f"Unable to create connection to '{target_name}'")

    # Verify
    assert hub.heist.CONS["asyncssh"][target_name].get("con")
    assert hub.heist.CONS["asyncssh"][target_name].get("sftp")

    # Cleanup, since nothing was accessed the connection will be hanging
    await hub.tunnel.asyncssh.destroy(target_name)


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.mark.asyncio
async def test_send(
    hub, basic_sftp_server: Tuple[int, str, str], temp_dir: str, target_name
):
    # Setup
    port, private_key, root = basic_sftp_server
    file_name = "send_example.text"
    dest_path = os.path.join(root, file_name)
    source_path = random_file(temp_dir, file_name)
    hub.heist.target.create(
        target_name,
        host=target_name,
        port=port,
        known_hosts=None,
        client_keys=[private_key],
    )
    assert hub.heist.TARGETS[target_name]

    if not await hub.tunnel.asyncssh.create(name=target_name):
        raise pytest.skip(f"Unable to create connection to '{target_name}'")

    # Execute
    # Send the temporary file to the root of the sftp server
    await hub.tunnel.asyncssh.send(name=target_name, source=source_path, dest=dest_path)

    # Verify
    assert os.path.exists(dest_path)
    await hub.tunnel.asyncssh.destroy(target_name)


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.mark.asyncio
async def test_get(
    hub, basic_sftp_server: Tuple[int, str, str], temp_dir: str, target_name
):
    # Setup
    port, private_key, root = basic_sftp_server
    file_name = "get_example.txt"
    # Create a file in the sftp root
    random_file(directory=root, name=file_name)
    dest_path = os.path.join(temp_dir, file_name)
    hub.heist.target.create(
        target_name,
        host=target_name,
        port=port,
        known_hosts=None,
        client_keys=[private_key],
    )
    if not await hub.tunnel.asyncssh.create(name=target_name):
        raise pytest.skip(f"Unable to create connection to '{target_name}'")

    # Execute
    await hub.tunnel.asyncssh.get(
        name=target_name, source=os.path.join(root, file_name), dest=dest_path
    )

    # Verify
    assert os.path.exists(dest_path)
    await hub.tunnel.asyncssh.destroy(target_name)


@pytest.mark.skipif(not HAS_SFTP, reason="Windows does not support SFTP module.")
@pytest.mark.asyncio
async def test_cmd(hub, sftp_ssh_server: Tuple[int, int, str], target_name):
    # Setup
    sftp_port, ssh_port, private_key = sftp_ssh_server
    command = "echo test"
    hub.heist.target.create(
        target_name,
        host=target_name,
        port=sftp_port,
        known_hosts=None,
        client_keys=[private_key],
    )
    assert hub.heist.TARGETS[target_name]
    if not await hub.tunnel.asyncssh.create(name=target_name):
        raise pytest.skip(f"Unable to create connection to '{target_name}'")

    # Execute
    ret = await hub.tunnel.asyncssh.cmd(name=target_name, command=command)

    # Verify
    assert ret.stdout.strip() == "test"

    await hub.tunnel.asyncssh.destroy(target_name)


@pytest.mark.asyncio
async def test_tunnel(hub, basic_sftp_server: Tuple[int, str, str], target_name):
    # Setup
    port, private_key, _ = basic_sftp_server
    remote = next(unused_ports())
    hub.heist.target.create(
        target_name,
        host=target_name,
        port=port,
        known_hosts=None,
        client_keys=[private_key],
    )
    assert hub.heist.TARGETS[target_name]
    if not await hub.tunnel.asyncssh.create(name=target_name):
        raise pytest.skip(f"Unable to create connection to '{target_name}'")

    # Execute
    await hub.tunnel.asyncssh.tunnel(name=target_name, local=port, remote=remote)

    # Verify
    reader, writer = await asyncio.open_connection("0.0.0.0", port)
    writer.write(b"GET / HTTP/1.0\r\n\r\n")
    local_port_response = await reader.read()

    reader, writer = await asyncio.open_connection("0.0.0.0", remote)
    writer.write(b"GET / HTTP/1.0\r\n\r\n")
    remote_port_response = await reader.read()

    assert local_port_response == remote_port_response

    # Cleanup
    await hub.tunnel.asyncssh.destroy(target_name)
