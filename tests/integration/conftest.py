import os
import socket
import unittest.mock as mock

import docker
import pytest


@pytest.fixture(name="roster", scope="function")
def get_roster() -> dict:
    """
    Return a temporary file that can be used as a roster
    """
    roster = {}
    yield roster


@pytest.fixture(scope="function")
def hub(hub, tmp_path):
    hub.pop.sub.add(dyne_name="heist")

    heist_conf = tmp_path / "heist.conf"
    with open(heist_conf, "w") as fp:
        pass

    with mock.patch("sys.argv", ["heist", "test", f"--config={heist_conf}"]):
        hub.pop.config.load(
            hub.heist.CONFIG_LOAD,
            cli="heist",
            parse_cli=False,
        )

    yield hub


def next_free_port(host, port: int = 2222) -> int:
    # Find an available port to bind to greater than 2222
    for i in range(100):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.settimeout(2)  # Timeout in case the port check hangs
            try:
                sock.bind((host, port))
            except OSError:
                port += 1  # If bind() raises an error, this port is unavailable
            else:
                break
    else:
        raise pytest.skip(f"Unable to find an available port on {host}")

    return port


@pytest.fixture(scope="function")
def openssh_container(roster):
    client = docker.from_env()

    if os.environ.get("GITLAB_CI") == "true":
        # For running in docker-in-docker on gitlab-ci
        network_name = "docker"
        host = "docker"
    else:
        # For running the tests locally
        network_name = "bridge"
        host = "localhost"

    port = next_free_port(host)

    container = client.containers.run(
        # https://hub.docker.com/r/linuxserver/openssh-server
        "linuxserver/openssh-server:latest",
        command=["/bin/sh", "-c", "while true; do sleep 1; done"],
        detach=True,
        ports={f"{port}/tcp": port},
        hostname=host,
        network=network_name,
        environment={
            "PUID": "1000",
            "PGID": "1000",
            "TZ": "Etc/UTC",
            "SUDO_ACCESS": "true",
            "PASSWORD_ACCESS": "true",
            "USER_NAME": "user",
            "USER_PASSWORD": "pass",
        },
    )

    try:
        container.reload()
        roster["heist_test"] = {
            "host": host,
            "id": host,
            "manage_service": False,
            "port": port,
            "username": "user",
            "password": "pass",
            "known_hosts": None,
            "bootstrap": True,
        }
        yield roster["heist_test"]

    finally:
        container.stop()
        container.remove()
