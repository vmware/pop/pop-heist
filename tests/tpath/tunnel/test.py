async def create(hub, name: str, **kwargs) -> bool:
    pass


async def send(hub, name: str, source: str, dest: str, **kwargs):
    pass


async def get(hub, name: str, source: str, dest: str):
    pass


async def cmd(hub, name: str, command: str, **kwargs):
    pass


async def tunnel(hub, name: str, remote: str, local: str):
    pass


async def destroy(hub, name: str):
    pass
