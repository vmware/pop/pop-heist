async def run(hub, target_name: str):
    hub.log.info(
        "This is a test heist manager. You have installed heist correctly. "
        "Install a heist manager to use full functionality"
    )
    return {
        "retvalue": 0,
    }


async def clean(hub, target_name: str):
    ...


def raw_run_cmd(hub, target_name: str):
    ...


def service_name(hub, target_name: str):
    ...


async def prepare_target(hub, target_name: str):
    ...
