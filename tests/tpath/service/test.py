def disable(hub, target_name):
    ...


def enable(hub, target_name):
    ...


def start(hub, target_name):
    ...


def stop(hub, target_name):
    ...


def restart(hub, target_name):
    ...


def clean(hub, target_name):
    ...


def status(hub, target_name):
    ...
