import sys
from unittest import mock

import pop.hub
import pytest


def mock_valid_service_names():
    return ("test-service",)


@pytest.fixture(scope="function")
def base_hub():
    hub = pop.hub.Hub()
    hub.pop.loop.create()
    yield hub


@pytest.fixture(scope="function")
def event_loop(base_hub):
    hub = base_hub
    hub.pop.sub.add(dyne_name="log")
    yield hub.pop.loop.CURRENT_LOOP
    hub.pop.loop.CURRENT_LOOP.close()


@pytest.fixture(scope="session")
def opt():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="heist")
    hub.pop.config.load(
        hub.heist.CONFIG_LOAD,
        cli="heist",
        parse_cli=False,
    )
    return hub.OPT


@pytest.fixture(scope="function")
async def hub(base_hub, target_name, opt):
    hub = base_hub
    hub.pop.sub.add(dyne_name="heist")
    hub.OPT = opt
    hub.heist.ROSTERS[target_name] = {}
    hub.heist.target.create(
        target_name,
        manager="test",
        remote={"manage_service": "test-service"},
        artifact_version=None,
        clean=False,
        connect=False,
    )
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    if sys.version_info < (3, 8):
        raise pytest.skip(
            "Mixing of real and test hubs is not supported in this version of Python"
        )
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    m_hub.OPT.update(hub.OPT)
    m_hub.SUBPARSER = "test"
    m_hub.SUBCOMMANDS = "test"
    m_hub.heist.TARGETS = hub.heist.TARGETS
    m_hub.heist.CONS = hub.heist.CONS
    m_hub.heist.ROSTERS = hub.heist.ROSTERS
    m_hub.pop.data.imap = hub.pop.data.imap
    m_hub.tunnel.init = hub.tunnel.init
    m_hub.service.init = hub.service.init

    for k, v in hub.lib._imports.items():
        setattr(m_hub.lib, k, v)

    yield m_hub
