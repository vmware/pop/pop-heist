#!/usr/bin/python3
import sys
import unittest.mock as mock

try:
    from unittest.mock import AsyncMock
except ImportError:
    # AsyncMock was first introduced in 3.8
    AsyncMock = None

from unittest.mock import call
from unittest.mock import create_autospec


import asyncssh
import pytest
from dict_tools.data import NamespaceDict
from tests.helpers import MockAsyncsshCon

import heist.tunnel.asyncssh as asyncssh_tunnel


def test__get_asyncssh_opt_target(mock_hub):
    """
    Test getting option from the target
    """
    mock_hub.OPT = {"heist": {"username", "opt"}}
    target = {"username": "target"}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "target"


def test__get_asyncssh_opt_config(mock_hub):
    """
    Test getting option from the config if target isn't available
    """
    mock_hub.OPT = NamespaceDict(heist={"username": "opt"})
    target = {}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "opt"


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_send(hub, mock_hub):
    """
    Test asyncssh.send
    """
    tunnel_name = "12345"
    src = "/tmp/test"
    dest = "/tmp/dest/test"
    mock_hub.tunnel.asyncssh.send = hub.tunnel.asyncssh.send
    mock_hub.heist.CONS = hub.heist.CONS
    mock_hub.heist.CONS["asyncssh"] = {
        tunnel_name: {"sftp": AsyncMock(asyncssh.sftp.SFTPClient), "sudo": False}
    }
    await mock_hub.tunnel.asyncssh.send(tunnel_name, src, dest)
    assert mock_hub.heist.CONS["asyncssh"][tunnel_name]["sftp"].put.call_args == call(
        src, dest, preserve=False, recurse=False
    )


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_cmd(hub, mock_hub):
    """
    Test asyncssh cmd
    """
    name = "12345"
    mock_hub.tunnel.asyncssh.cmd = hub.tunnel.asyncssh.cmd
    mock_hub.heist.CONS = hub.heist.CONS
    mock_hub.heist.CONS["asyncssh"] = {name: {"sudo": False, "con": AsyncMock()}}
    mock_hub.heist.TARGETS = hub.heist.TARGETS
    hub.heist.TARGETS[name] = {"os": "linux"}
    await mock_hub.tunnel.asyncssh.cmd(name, "echo test")
    assert mock_hub.heist.CONS["asyncssh"][name]["con"].run.call_args_list == [
        mock.call("echo test")
    ]


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_sudo_cmd(hub):
    """
    Test asyncssh cmd when sudo is True.
    """
    name = "12345"
    hub.heist.TARGETS[name] = {"os": "linux", "sudo": True}
    patch_client = mock.patch.object(
        asyncssh.connection, "SSHClientConnection", new_callable=MockAsyncsshCon
    )
    with patch_client as test:
        hub.heist.CONS["asyncssh"] = {name: {"con": test, "password": "securepassword"}}
        ret = await hub.tunnel.asyncssh.cmd(name, "echo test")
    assert ret.stdout == "test"
    assert ret.returncode == 1


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_sudo_passwordless_cmd(hub):
    """
    Test asyncssh cmd when sudo is True.
    """
    name = "12345"
    hub.heist.TARGETS[name] = {"os": "linux"}
    mock_ssh_client = create_autospec(asyncssh.connection.SSHClientConnection)
    mock_ssh_client.run.return_value = NamespaceDict(stdout="test", returncode=1)

    # Create a function that replaces SSHClientConnection with a mock object
    # Use patch() to replace SSHClientConnection with your mock_ssh_client function
    with mock.patch(
        "asyncssh.connection.SSHClientConnection", new=mock_ssh_client
    ) as test:
        hub.heist.CONS["asyncssh"] = {
            name: {"sudo": True, "con": test, "password": "securepassword"}
        }
        ret = await hub.tunnel.asyncssh.cmd(name, "echo test")
    assert ret.stdout == "test"
    assert ret.returncode == 1
