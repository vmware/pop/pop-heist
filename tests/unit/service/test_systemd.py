import sys
from pathlib import PurePosixPath
from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_start_service(hub, mock_hub, target_name):
    """
    Test starting a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.start = hub.service.systemd.start
    await mock_hub.service.systemd.start(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl start {service_name}"
    )


@pytest.mark.asyncio
async def test_start_service_block_is_false(hub, mock_hub, target_name):
    """
    Test starting a systemd service when block is False
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    hub.heist.TARGETS[target_name]["block"] = False
    mock_hub.service.systemd.start = hub.service.systemd.start
    await mock_hub.service.systemd.start(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl start {service_name} --no-block"
    )


@pytest.mark.asyncio
async def test_disable_service(hub, mock_hub, target_name):
    """
    Test disabling a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.disable = hub.service.systemd.disable
    await mock_hub.service.systemd.disable(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl disable {service_name}"
    )


@pytest.mark.asyncio
async def test_enable_service(hub, mock_hub, target_name):
    """
    Test enabling a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.enable = hub.service.systemd.enable
    await mock_hub.service.systemd.enable(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl enable {service_name}"
    )


@pytest.mark.asyncio
async def test_enable_service(hub, mock_hub, target_name):
    """
    Test stopping a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.enable = hub.service.systemd.enable
    await mock_hub.service.systemd.enable(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl enable {service_name}"
    )


@pytest.mark.asyncio
async def test_stop_service(hub, mock_hub, target_name):
    """
    test stopping a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.stop = hub.service.systemd.stop
    await mock_hub.service.systemd.stop(
        target_name,
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl stop {service_name}"
    )


@pytest.mark.asyncio
async def test_restart_service(hub, mock_hub, target_name):
    """
    test restarting a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.restart = hub.service.systemd.restart
    await mock_hub.service.systemd.restart(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl restart {service_name}"
    )


@pytest.mark.skipif(
    sys.platform == "win32",
    reason="path_convert function not supported by Windows yet.",
)
def test_conf_path(hub, mock_hub, target_name):
    """
    test conf_path when target is linux
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.conf_path = hub.service.systemd.conf_path
    mock_hub.tool.heist.path.remote = hub.tool.heist.path.remote
    ret = mock_hub.service.systemd.conf_path(target_name)
    assert ret == PurePosixPath("/etc", "systemd", "system", "test-service.service")


@pytest.mark.asyncio
async def test_clean(hub, mock_hub, target_name):
    """
    test cleaning a systemd service
    """
    service_name = "minion"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.clean = hub.service.systemd.clean
    await mock_hub.service.systemd.clean(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, "systemctl daemon-reload"
    )


@pytest.mark.asyncio
async def test_status_service(hub, mock_hub, target_name):
    """
    test getting status of a systemd service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.systemd.status = hub.service.systemd.status
    await mock_hub.service.systemd.status(target_name=target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl status {service_name}"
    )
