from unittest.mock import call
from unittest.mock import Mock

import pytest


@pytest.mark.asyncio
async def test_start_service(hub, mock_hub, target_name):
    """
    Test starting a raw service
    """
    hub.heist.TARGETS[target_name].update(
        dict(
            run_cmd="/path/to/run -c /path/to/config",
        )
    )
    mock_hub.service.raw.start = hub.service.raw.start
    mock_hub.tool.heist.service = Mock()
    await mock_hub.service.raw.start(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name,
        "/path/to/run -c /path/to/config",
        background=True,
    )


@pytest.mark.asyncio
async def test_stop_service(hub, mock_hub, target_name):
    """
    Test stopping a raw service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name].update(
        dict(
            service_name=service_name,
            os="linux",
        )
    )
    mock_hub.service.raw.stop = hub.service.raw.stop
    mock_hub.tool.heist.system.os_arch.return_value = ("linux", "amd64")
    await mock_hub.service.raw.stop(target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"pkill -f {service_name}"
    )


@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_status_service(hub, mock_hub, target_os, target_name):
    """
    Test getting the status of a raw service
    """
    service_name = "test-service"
    mock_hub.service.raw.status = hub.service.raw.status
    mock_hub.tool.heist.system.os_arch.return_value = (target_os, "amd64")
    hub.heist.TARGETS[target_name].update(
        dict(
            service_name=service_name,
            os=target_os,
        )
    )
    await mock_hub.service.raw.status(target_name=target_name)
    exp_cmd = f"pgrep -f {service_name}"
    if target_os == "windows":
        exp_cmd = f"get-process {service_name}"
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(target_name, exp_cmd)


@pytest.mark.asyncio
async def test_restart_service(hub, mock_hub, target_name):
    """
    Test restarting a raw service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name].update(
        dict(
            service_name=service_name,
        )
    )
    mock_hub.service.raw.restart = hub.service.raw.restart
    mock_hub.tool.heist.system.os_arch.return_value = ("linux", "amd64")
    assert await mock_hub.service.raw.restart(target_name)
    mock_hub.service.raw.stop.assert_called()
    mock_hub.service.raw.status.await_count == 2
    mock_hub.service.raw.status.assert_called()
    mock_hub.service.raw.start.assert_called()
