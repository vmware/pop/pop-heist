from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_status_service(hub, mock_hub, target_name):
    """
    Test getting status of a windows service
    """
    service_name = "test-service"
    hub.heist.TARGETS[target_name]["service_name"] = service_name
    mock_hub.service.win_service.status = hub.service.win_service.status
    await mock_hub.service.win_service.status(target_name=target_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"Get-Service {service_name}"
    )
