import subprocess
import sys

import pytest


@pytest.mark.parametrize(
    "target_os,exp_ret", [("windows", ".zip"), ("linux", "tar.xz")]
)
def test_get_artifact_suffix(hub, mock_hub, target_os, exp_ret):
    """
    Test function get_artifact_suffix
    for linux and windows
    """
    mock_hub.tool.heist.artifacts.get_artifact_suffix = (
        hub.tool.heist.artifacts.get_artifact_suffix
    )
    ret = mock_hub.tool.heist.artifacts.get_artifact_suffix(target_os=target_os)
    assert ret == exp_ret


async def test_artifact_dir_permissions(mock_hub, hub, tmp_path, target_name):
    """
    test the correct permissions are set when
    creating the artifacts directory.
    """
    artifacts_dir = tmp_path / "artifacts"
    mock_hub.heist.remotes.run = hub.heist.remotes.run
    mock_hub.heist.clean.run = hub.heist.clean.run
    mock_hub.heist.target.create = hub.heist.target.create
    mock_hub.heist.manager.run = hub.heist.manager.run
    mock_hub.heist.system.os_arch.return_value = ("", "")
    mock_hub.heist.TARGETS.pop(target_name)

    await mock_hub.heist.remotes.run(
        manager="test",
        remotes={target_name: {"value": "test2", "artifacts_dir": artifacts_dir}},
    )
    if sys.platform == "win32":
        ret = (
            subprocess.run(["cacls", str(artifacts_dir)], capture_output=True)
            .stdout.decode()
            .split("\n")
        )
        assert "BUILTIN\\Users" in ret[0]
        assert "READ_CONTROL" in ret[1]
    else:
        assert oct(artifacts_dir.stat().st_mode) == "0o40600"


assert isinstance(pytest.mark.asyncio, object)
