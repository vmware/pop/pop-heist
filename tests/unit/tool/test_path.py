"""
    tests.unit.tool.test_path
    ~~~~~~~~~~~~~~

    tests for tool.heist.path code
"""
import pathlib
import sys

import pytest


@pytest.mark.skipif(sys.platform == "win32", reason="Does not run on Windows.")
@pytest.mark.parametrize(
    "root,path,exp_ret",
    [
        ("/etc", "test", "/etc/test"),
        ("/var", "/etc/var/tmp", ""),
        ("/etc/", "../../../", ""),
    ],
)
def test_clean_path_linux(mock_hub, hub, root, path, exp_ret):
    """
    test clean_path on linux
    """
    mock_hub.tool.heist.path.clean = hub.tool.heist.path.clean
    mock_hub.tool.heist.path.real = hub.tool.heist.path.real
    assert mock_hub.tool.heist.path.clean(root, path) == exp_ret


@pytest.mark.skipif(sys.platform != "win32", reason="Only runs on Windows.")
@pytest.mark.parametrize(
    "root,path,exp_ret",
    [
        ("c:\\tmp", "test", "c:\\tmp\\test"),
    ],
)
def test_clean_path_windows(mock_hub, hub, root, path, exp_ret):
    """
    test clean_path on windows
    """
    mock_hub.tool.heist.path.clean = hub.tool.heist.path.clean
    assert mock_hub.tool.heist.path.clean(root, path) == exp_ret


@pytest.mark.parametrize(
    "os_name,root,path,exp_ret",
    [
        (
            "linux",
            "/etc",
            ["test", "this", "path"],
            pathlib.PurePosixPath("/etc/test/this/path"),
        ),
        (
            "win",
            "c://etc",
            ["test", "this", "path"],
            pathlib.PureWindowsPath("c:/etc/test/this/path"),
        ),
    ],
)
def test_path_convert(mock_hub, hub, os_name, root, path, exp_ret):
    mock_hub.tool.heist.path.remote = hub.tool.heist.path.remote
    ret = mock_hub.tool.heist.path.remote(os_name, root, path)
    assert ret == exp_ret
