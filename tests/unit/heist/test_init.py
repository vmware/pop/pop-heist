#!/usr/bin/python3
"""
    tests.unit.heist.test_heist
    ~~~~~~~~~~~~~~

    tests for heist.init code
"""
import pathlib
import sys
import unittest.mock as mock

import pytest
from dict_tools.data import NamespaceDict

import tests.helpers


@pytest.mark.skipif(sys.platform == "win32", reason="Does not run on Windows.")
def test_start_nix(hub, mock_hub):
    mock_hub.heist.loop.start = hub.heist.loop.start
    mock_hub.heist.init.cli = hub.heist.init.cli
    mock_hub.OPT.rend.output = "yaml"
    mock_hub.heist.init.cli()
    mock_hub.heist.remotes.run_roster.assert_called_once()


@pytest.mark.asyncio
async def test_run_remotes(mock_hub, hub, tempdir):
    mock_hub.heist.remotes.run_roster = hub.heist.remotes.run_roster
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "test"}

    await mock_hub.heist.remotes.run_roster("test", roster_file=tempdir)

    mock_hub.roster.init.read.assert_called_once_with(
        None, roster_file=tempdir, roster_data=None
    )


@pytest.mark.parametrize(
    "addr",
    [
        ("127.0.0.1", True),
        ("::1", True),
        ("2001:0db8:85a3:0000:0000:8a2e:0370:7334", False),
        ("localhost", True),
        ("1.1.1.1", False),
        ("google.com", False),
    ],
)
def test_ip_is_loopback(hub, addr):
    """
    Test for function ip_is_loopback
    when socket error raised, expected
    return is False
    """
    ret = hub.tool.heist.ip_addr.is_loopback(addr[0])
    assert ret == addr[1]


def test_ip_is_loopback_exception(hub):
    """
    Test for function ip_is_loopback
    when address is not valid
    """
    assert not hub.tool.heist.ip_addr.is_loopback("")


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_clean(hub, tempdir):
    """
    test cleanup
    """
    con1 = "123456789"
    con2 = "987654321"
    hub.heist.CONS["asyncssh"] = {
        con1: {"manager": "test", "con": None},
        con2: {"manager": "test", "con": None},
    }
    hub.heist.ROSTERS = {
        con1: {"host": "192.168.1.1"},
        con2: {"host": "192.168.1.2"},
    }
    hub.heist.TARGETS = {
        con1: {"tunnel_plugin": "asyncssh"},
        con2: {"tunnel_plugin": "asyncssh"},
    }
    hub.OPT = NamespaceDict(heist={"clean": False})
    hub.heist.clean.run = hub.heist.clean.run
    hub.manager.test.clean = hub.manager.test.clean
    hub.lib.asyncio.gather = hub.lib.asyncio.gather

    await hub.heist.clean.run(con1)
    await hub.heist.clean.run(con2)


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_clean_noclean_set(mock_hub, hub, tempdir, target_name):
    """
    test cleanup when noclean is set
    """
    mock_hub.heist.CONS = {"asyncssh": {target_name: {"manager": "test"}}}
    mock_hub.heist.ROSTERS = {target_name: {"manager": "test"}}
    mock_hub.OPT = NamespaceDict(heist={"clean": False})
    mock_hub.heist.clean.run = hub.heist.clean.run
    tests.helpers.mock_manager(mock_hub)
    await mock_hub.heist.clean.run()
    mock_hub.manager.test.clean.assert_not_called()


@pytest.mark.skipif(True, reason="Skipped until refactor")
def test_config_paths(mock_hub, hub, tmp_path):
    """
    test paths in config
    """
    mock_hub.pop.config.load = hub.pop.config.load
    heist_conf = tmp_path / "heist.conf"
    heist_conf.write_text("")
    with mock.patch("sys.argv", ["heist", "test", f"-c={heist_conf}"]):
        hub.pop.config.load(hub.heist.CONFIG_LOAD, cli="heist")
        if sys.platform == "linux":
            linux_root = pathlib.Path("/etc", "heist")
            assert hub.OPT.heist.config == str(heist_conf)
            assert hub.OPT.heist.roster_dir == str(linux_root / "rosters")
            assert hub.OPT.heist.artifacts_dir == str(
                pathlib.Path("/var", "tmp", "heist", "artifacts")
            )
        elif sys.platform == "win32":
            windows_root = pathlib.Path("C:\\ProgramData", "heist")
            assert hub.OPT.heist.config == str(heist_conf)
            assert hub.OPT.heist.roster_dir == str(windows_root / "rosters")
            assert hub.OPT.heist.artifacts_dir == str(windows_root / "artifacts")
        else:
            raise AssertionError("Did not find OS. Need to add tests for designated OS")


@pytest.mark.parametrize(
    "root_opts,exp_path",
    [
        (None, "/var/tmp/"),
        ("/tmp", "/tmp"),
    ],
)
def test_config_env(mock_hub, hub, tmp_path, root_opts, exp_path):
    mock_hub.pop.config.load = hub.pop.config.load
    mock_hub.heist.system.get_default = hub.heist.system.get_default
    run_dir_root = mock_hub.heist.system.get_default(
        "run_dir_root", target_os="linux", run_dir_root=root_opts
    )
    assert run_dir_root == exp_path


@pytest.mark.skipif(
    sys.version_info < (3, 8), reason="cli_runpy fixture not available on python 3, 8"
)
def test_start_no_manager_nix(cli_runpy):
    """
    Test the start method without defining a manager
    """
    ret = cli_runpy("--log-level=error", parse_output=False, check=False)
    assert "Heist must be run with a manager" in ret.stderr
