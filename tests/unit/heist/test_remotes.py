import unittest.mock as mock

import pytest


@pytest.mark.parametrize(
    "clean",
    [True, False],
)
@pytest.mark.asyncio
async def test_run_remotes_clean(
    mock_hub,
    hub,
    tempdir,
    clean,
):
    """
    test run_remotes when clean is passed in
    """
    mock_hub.heist.remotes.run_roster = hub.heist.remotes.run_roster
    mock_hub.heist.remotes.run = hub.heist.remotes.run
    mock_hub.heist.target.create = hub.heist.target.create
    mock_hub.heist.manager.run = hub.heist.manager.run
    mock_hub.heist.system.os_arch.return_value = ("", "")
    mock_hub.manager.test = hub.manager.test
    mock_hub.roster.init.read.return_value = {"test": {"value": "test2"}}
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "test"}
    await mock_hub.heist.remotes.run_roster("test", tempdir, clean=clean)
    assert mock_hub.heist.TARGETS["test"]["clean"] is clean


@pytest.mark.asyncio
async def test_run_remotes_return(mock_hub, hub, tempdir):
    """
    Test running run_remotes and return a success
    """
    exp_ret = [
        {
            "result": "Success",
            "comment": "This is a comment",
            "retvalue": 0,
            "target": "test",
        }
    ]
    mock_hub.heist.remotes.run_roster = hub.heist.remotes.run_roster
    mock_hub.heist.remotes.run = hub.heist.remotes.run
    mock_hub.heist.target.create = hub.heist.target.create
    mock_hub.manager.test.run = hub.manager.test.run
    mock_hub.heist.system.os_arch.return_value = ("", "")
    mock_hub.roster.init.read.return_value = {"test": {"value": "test2"}}
    mock_hub.OPT.update(dict(heist={"roster": mock.sentinel.roster, "manager": "test"}))
    mock_hub.manager.test.run.return_value = exp_ret
    mock_hub.heist.manager.run = hub.heist.manager.run

    ret = await mock_hub.heist.remotes.run_roster("test", tempdir)
    assert ret == exp_ret

    mock_hub.roster.init.read.assert_called_once_with(
        None, roster_file=tempdir, roster_data=None
    )
