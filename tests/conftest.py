import pytest


@pytest.fixture(scope="function")
def target_name() -> str:
    yield "0.0.0.0"
