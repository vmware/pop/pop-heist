All notable changes to Heist will be documented in this file.

This changelog follows [keepachangelog](https://keepachangelog.com/en/1.0.0/) format, and is intended for human consumption.

# Changelog
Heist 6.3.1 (2023-05-18)
========================

Fixed
-----

- Fixed sudo passwordless. You can now use passwordless or password based sudo. (#152)


Heist 6.3.0 (2023-05-03)
========================

Changed
-------

- Changed the artifact.init.extract from extracting the artifact on the host where heist is running to extract it on the target. This has resulted in an inrease in speed since we are copying over a smaller artifact before it is extracted. (#138)


Fixed
-----

- Fix the permissions for the artifacts directory. (#116)
- add config option for run_dir_root (#120)
- Explicitly pass in the aliases dir when deploying the aliases. (#135)
- Print the error or success from the returned data with red or green colors. (#137)
- When using the raw service plugin, we now check to see if the service is running before attempting to stop it during a restart. (#143)
- Fixed error when using `run_dir_root` for a target. (#144)
- Allow heist to run against windows minions with both CMD or powershell as the default shell. (#151)


Added
-----

- Add ability to check if artifact is already deployed and verify the artifact. (#95)
- If a previous artifact was already deployed, return the name of the artifact. (#139)
- Add ability to create and deploy aliases (#140)
- Allow Heist to manage service of a previously deployed artifact (#141)
- Add new option `--clean` to clean the deployed artifact before deploying a new one. (#142)


Heist v6.2.0 (2022-07-12)
=========================

Fixed
-----

- Add tty option to fix sudo user not able to authenticate without a tty. (#124)


Added
-----

- Add raw service so artifacts can be run without a service manager. This also changed the default service manager to raw, instead of systemd. (#77)
- Added Windows support for raw plugin (#127)
- Added win_service plugin for managing Windows Services (#132)


Heist v6.1.0 (2021-12-07)
=========================

Added
-----

- Add recurse argument for asyncssh.send to allow user to copy recursively a directory. (#123)


Heist v6.0.1 (2021-11-04)
=========================

Fixed
-----

- Fix cleanup when targeting multiple hosts. (#121)


Heist v6.0.0 (2021-10-25)
=========================

Removed
-------

- The windows default configuration paths have been moved from `C:\heist\` to `C:\ProgramData\heist\`. (#100)


Deprecated
----------

- The default Windows configuration paths will change from C:\heist to C:\ProgramData\heist in Heist version v6.0.0. (#113)


Fixed
-----

- Allow heist to use environment variable to set config file. (#117)
- Fix traceback when using config artifacts_dir. (#118)
- Enforce the flat roster to be the default. (#119)


Added
-----

- Add towncrier tool to the heist project to help manage CHANGELOG.md file. (#110)
- Add a test Heist manager for users to test Heist installation and setup. (#114)
- Migrate artifact calls to heist/artifact/init.py (#115)
